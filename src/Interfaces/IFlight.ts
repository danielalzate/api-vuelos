import { IAirport } from '../Interfaces/IAirport'
export interface IFlight {
    origin: IAirport,
    destination: IAirport,
    images: [string],
    price: number,
    segments: [IAirport]
}