export interface IAirport {
    iata: string,
    city: string,
    country: string
}