import { Flight } from '../models/schema/Flight'
import { Airport } from '../models/schema/Airport'
export const Services = {
    home: async () => {
        return 'Service Home'
    },
    createAirport: async (airport: any) => {
        try {
            const data = await Airport.create(Object.assign({}, airport))
            return data
        } catch (error) {
            return `Error al crear un aeropuerto ${error}`
        }
    },
    allAirports: async () => {
        try {
            const data = await Airport.find()
            return data
        } catch (error) {
            return `Error al consultar aeropuertos ${error}`
        }
    },
    Createflight: async (flight: any) => {
        try {
            const data = await Flight.create(Object.assign({}, flight))
            return data
        } catch (error) {
            return `Error al crear un vuelo ${error}`
        }
    },
    allFlights: async () => {
        try {
            const data = await Flight.find()
            return data
        } catch (error) {
            return `Error al consultar vuelos ${error}`
        }
    },
    flights: async (params: any) => {
        try {
            const { origin, destination } = params
            const data = await Flight.find({
                'origin.iata': origin, 'destination.iata': destination
            })
            return data
        } catch (error) {
            return `Error al consultar vuelos ${error}`
        }
    },
    flightRoundTrip: async (params: any) => {
        try {
            const { origin, destination } = params
            const going = await Services.flights(
                { origin: origin, destination: destination })
            const backing = await Services.flights(
                { origin: destination, destination: origin })
            const roundTrip = { go: going, back: backing }
            return roundTrip
        } catch (error) {
            return `Error al consultar vuelos ida y vuelta ${error}`
        }
    },
    multiDestination: async (params: any) => {
        try {
            const { flights } = params
            let multiFlights: any = {}
            for (let i = 0; i < flights.length; i++) {
                multiFlights[`${i}`] = await Services.flights(flights[i])
            }
            return multiFlights
        } catch (error) {
            return `Error al consultar vuelos multiples ${error}`
        } 
    }
}