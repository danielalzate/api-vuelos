# ApiVuelos

## Que es

ApiVuelos es una api rest que permite resolver las pediciones de ciudades y vuelos.

# Esquemas
* Airport
* Bar : [nombre, ubicacion, genero, calificacion, musica, cover, info, social, horario, capacidad, contenido]
* Flight

# EndPoints

*Home: '/'
*createAirport '/createAirport'
*allAirports: '/allAirports'
*createFlight: '/createFlight',
*allFlights: '/allFlights',
*flights:  '/flights'
*flightRoundTrip:  '/flightRoundTrip',
*multiDestination: '/multiDestination',
