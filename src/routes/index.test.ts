import express from 'express';
import bodyParser from 'body-parser';
import request from 'supertest';
import { Routes } from './index';
import { middleware } from '../middlewares'

describe('Loading server express', () => {
    const app = express();

    Object.keys(Routes).forEach((key) => {
        app[Routes[key].verb](Routes[key].uri, Routes[key].action);
    });

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json())
    // app.set('Content-Type', 'application/json');
    // app.set('Accept', 'application/json');

    it('It responds to /customer', () => {


        return request(app).get('/hotels').then((res) => console.log(res));

    });


    // it('It get customer by id', () => {
    //     return request(app).get('/customer/5ace5664d56c02761960f50b').then((res) => expect(objCustomer).toEqual(res.body.customer));
    // });
    // // it('It save customer', () => {
    // //     return request(app).post('/customer')
    // //         .type('form')
    // //         .then((res) => expect(objCustomerSave).toEqual(res.body));
    // // });
    // it('It get customer by id with https of node', () => {
    //     return request(app).get('/customer/5ace5664d56c02761960f50b').then((res) => expect(objCustomer).toEqual(res.body.customer));
    // });
});