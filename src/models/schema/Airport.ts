import { Document, Schema, Model, model } from 'mongoose'
import { IAirport } from '../../interfaces/IAirport'

export interface AirportModel extends IAirport,Document { }
export const AirportSchema: Schema = new Schema({
    iata: {
        required: true,
        type: String
    },
    city: {
        required: true,
        type: String,
        default: 'New City'
    },
    country: {
        required: true,
        type: String,
        default: 'New Country'
    }
}, { timestamps: true, versionKey: false })
export const Airport: Model<AirportModel> = model<AirportModel>('airports', AirportSchema)