import { Controller } from '../controllers'

export const Routes: { [index: string]: any } = {
    Home: {
        verb: 'get',
        uri: '/',
        action: Controller.home
    },
    createAirport: {
        verb: 'post',
        uri: '/createAirport',
        action: Controller.createAirport
    },
    allAirports: {
        verb: 'get',
        uri: '/allAirports',
        action: Controller.allAirports
    },
    createFlight: {
        verb: 'post',
        uri: '/createFlight',
        action: Controller.Createflight
    },
    allFlights: {
        verb: 'get',
        uri: '/allFlights',
        action: Controller.allFlights
    },
    flights: {
        verb: 'get',
        uri: '/flights',
        action: Controller.flights
    },
    flightRoundTrip: {
        verb: 'get',
        uri: '/flightRoundTrip',
        action: Controller.flightRoundTrip
    },
    multiDestination: {
        verb: 'get',
        uri: '/multiDestination',
        action: Controller.multiDestination
    }
    
}