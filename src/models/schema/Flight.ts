import { Document, Schema, Model, model } from 'mongoose'
import { IFlight } from '../../interfaces/IFlight'
import { AirportSchema } from './Airport'

export interface FlightModel extends IFlight, Document { }
export const FlightSchema: Schema = new Schema({
    origin: {
        required: true,
        type: AirportSchema
    },
    destination: {
        required: true,
        type: AirportSchema,
    },
    images: {
        required: true,
        type: [String],
        default: ['https://increasify.com.au/wp-content/uploads/2016/08/default-image.png0']
    },
    price: {
        required: true,
        type: Number,
        default: 0
    },
    segments: {
        required: true,
        type: [AirportSchema],
    }
}, { timestamps: true, versionKey: false })
export const Flight: Model<FlightModel> = model<FlightModel>('flights', FlightSchema)