import { Services } from '../services'
import { Response, Request } from 'express'
export const Controller = {
    home: async (req: Request, res: Response) => {
        const data = await Services.home()
        res.send(data)
    },
    createAirport: async (req: Request, res: Response) => {
        const data = await Services.createAirport(req.body)
        res.send(data)
    },
    allAirports: async  (req: Request, res: Response) => {
        const data = await Services.allAirports()
        res.send(data)
    },
    Createflight: async (req: Request, res: Response) => {
        const data = await Services.Createflight(req.body)
        res.send(data)
    },
    allFlights: async  (req: Request, res: Response) => {
        const data = await Services.allFlights()
        res.send(data)
    },
    flights: async (req: Request, res: Response) => {
        const data = await Services.flights(req.query)
        res.send(data)
    },
    flightRoundTrip: async (req: Request, res: Response) => {
        const data = await Services.flightRoundTrip(req.query)
        res.send(data)
    },
    multiDestination: async (req: Request, res: Response) => {
        const data = await Services.multiDestination(req.query)
        res.send(data)
    }
}